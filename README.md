# Jasper Releases

v15

## Run node TESTNET


```bash
jasperchaind jasper15@warpit.tarpat.com:15222 -daemon
```

## Run client
```
jasperchain-cli jasper15
```


### Methods

- getinfo
- getpeerinfo
- getaddresses
- getbalance

https://www.multichain.com/developers/json-rpc-api/